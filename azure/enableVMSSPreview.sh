#!/bin/bash

az extension add --name aks-preview
az feature register --namespace Microsoft.ContainerService --name VMSSPreview
az provider register -n Microsoft.ContainerService